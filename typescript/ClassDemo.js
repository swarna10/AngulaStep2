var Car = /** @class */ (function () {
    function Car(user) {
        this.user = user;
    }
    Car.prototype.display = function () {
        console.log("Hi," + this.user);
    };
    return Car;
}());
var obj = new Car("John");
obj.display();
