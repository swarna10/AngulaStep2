class Vehicle
{
    color : any ;
    constructor(color:String)
    {
        this.color = color ;    
    }
}
class Shape extends Vehicle
{
    dis() : void
    {
        console.log("Engine color is "+this.color);
    }
}
var obj1 = new Shape("black");
obj1.dis();