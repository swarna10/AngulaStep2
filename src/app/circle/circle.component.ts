import { Component, OnInit, Output ,EventEmitter} from '@angular/core';
import { Circle } from './circle' ;
import {CircleService} from '../circle.service' ;

@Component({
  selector: 'app-circle',
  templateUrl: './circle.component.html',
  styleUrls: ['./circle.component.css']
})
export class CircleComponent implements OnInit {
  circles : Circle [] ;
  @Output() selectedCircle = new EventEmitter<any>();
  
  constructor(private circleservice : CircleService) { }
  selectCircle(circledata : String)
  {
    const currentvalue = {
      type : 'circle',
      value : circledata 
    };
    return this.selectedCircle.emit(currentvalue);
  }

  getCircles()
  {
    return this.circleservice.getMessages().subscribe(data=>
    {
      this.circles = data.json();
    });
  }

  ngOnInit()
   {
   return this.getCircles();
  }

}

// import { Component, OnInit } from '@angular/core';
// import { Circle , CIRCLE } from './circle' ;
// @Component({
//   selector: 'app-circle',
//   templateUrl: './circle.component.html',
//   styleUrls: ['./circle.component.css']
// })
// export class CircleComponent implements OnInit {
//   constructor() { }
// circles : Circle [] ;
//   ngOnInit()
//    {
//      this.circles = CIRCLE ;
//   }
// }
