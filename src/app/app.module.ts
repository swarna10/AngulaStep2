import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { RouterModule} from '@angular/router';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { MessageComponent } from './message/message.component';
import { CircleComponent } from './circle/circle.component';
import { HttpModule } from '@angular/http';

import {UserService} from './user.service' ;
import {CircleService} from './circle.service' ;
import { MessageService} from './message.service' ;

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    MessageComponent,
    CircleComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      {
        path : 'user' ,
        component : UserComponent,
      },
      {
        path : 'circle' ,
        component : CircleComponent,
      },
      {
        path : 'message' ,
        component : MessageComponent,
      }

    ])
  ],
  providers: [UserService,CircleService,MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
