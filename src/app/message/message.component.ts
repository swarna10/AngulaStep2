import { Component, Output,OnInit,EventEmitter } from '@angular/core';
import { Message } from './message' ;
import {MessageService} from '../message.service' ;


@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
 messages : Message[] ;
@Output() selectedMessage = new EventEmitter<any>();

  constructor(private messageservice: MessageService) { }
  selectMessage(messageData : String)
  {
    const currentMessage = {
      type : 'message',
      value : messageData
    }
    return this.selectedMessage.emit(currentMessage);
  }

  getMessages()
  {
    this.messageservice.getMessages().subscribe(data=>
    {
      this.messages = data.json();
    });
  }

  ngOnInit() 
  {
    this.getMessages();
  }

}

// import { Component, OnInit } from '@angular/core';
// import { Message , MESSAGE } from './message' ;
// @Component({
//   selector: 'app-message',
//   templateUrl: './message.component.html',
//   styleUrls: ['./message.component.css']
// })
// export class MessageComponent implements OnInit {
//   constructor() { }
// messages : Message[] ;
//   ngOnInit() 
//   {
//     this.messages = MESSAGE ;
//   }
// }
