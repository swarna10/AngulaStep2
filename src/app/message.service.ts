import { Injectable } from '@angular/core';
import {Message} from './message/message' ;
import {Http} from '@angular/http' ;

@Injectable()
export class MessageService {

  constructor(private http: Http) { }
  private MESSAGE_SERVICE_BASE_URL = "http://localhost:28200/api/message" ;

  getMessages()
  {
    return this.http.get(this.MESSAGE_SERVICE_BASE_URL) ;
  }

}
